# Projetos
Repositório para expor os meus projetos de análise e visualização de dados, machine learning e deep learning

### Descrição dos projetos

------------------------------
**<p>Código: 1_Project</p>**
Descrição: Competição no Kaggle para prever 3 meses de vendas de itens em diferentes lojas. Foram utilizados os modelos Neural Network Autoregression, LightGBM e o modelo univariado de suavização exponencial.

O modelo com a utilização do LightGBM alcançou a 10a colocação na competição, dentre 460 participantes.
![](https://github.com/guilhermeagsouza/ImagensTabelasDosProjetos/blob/master/kaggle_score.PNG)

Neste [link](https://www.kaggle.com/guilhermeagsouza/neural-network-autoregression-15-73290) você consegue ver a implementação do modelo de redes neurais com regressores. 

------------------------------
 **<p>Código: 2_Project</p>**
 Descrição: Neste projeto analisamos dados diários da concentração de monóxido de carbono de estações de monitoramento da qualidade do ar, obtidos através do site [Data.Rio](http://www.data.rio). Foi realizada uma análise exploratória (EDA) e detecção de anomalias, além da previsão de quatro modelos distintos, sendo eles o primeiro o de suavização exponencial Holt-Winters, o segundo Prophet (desenvolvido pelo Facebook), o terceiro Redes Neurais (Hyndman) e o modelo ARIMA. Utilizou-se validação cruzada (cross-validation) para cálculo dos *mapes* (mean absolute percentage error), métrica para avaliação da acurácia dos modelos. 
 
 O código está disponível neste [link](https://github.com/guilhermeagsouza/Projetos/blob/master/2_Project.md).
 
 ------------------------------
 **<p>Código: 3_Project</p>**
Descrição: Neste projeto estudo indicadores econômicos, de educação, população e infraestrutura que mais impactam em medidas que possam auxiliar municípios de São Paulo em metas de redução de óbito e acidentes em estradas, para o ano de 2017. Foram utilizadas técnicas de imputação de dados com Random Forest, modelo de regressão para avaliar quais variáveis eram significativas e por final a técnica de clusterização para identificar quais variáveis são mais associadas a um grupo de municípios. 

 O código está disponível neste [link](https://github.com/guilhermeagsouza/Projetos/blob/master/3_Project.md).
 
 ------------------------------
**<p>Dashboard para Gestão de Combustível feito em Shiny</p>**
Descrição: Neste [link](https://guilhermeagsouza.shinyapps.io/gestao_combustivel_beta/) está disponível um dashboard criado por mim para analisar indicadores de gestão de combustível de diversos níveis hierárquicos de uma companhia. O dashboard está compartilhado no site [ShinyApps](https://www.shinyapps.io).
